import { useState } from 'react';

export default contacts => {
    const [sorting, setSorting] = useState('asc');

    const toggleSorting = () => {
        if (sorting === 'asc') {
            setSorting('desc');
        } else {
            setSorting('asc');
        }
    };

    const sortedContacts = contacts.slice().sort((a, b) => {
        if (sorting === 'asc') {
            if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;
            if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;
            return 0;
        } else {
            if (a.name.toLowerCase() > b.name.toLowerCase()) return -1;
            if (a.name.toLowerCase() < b.name.toLowerCase()) return 1;
            return 0;
        }
    });

    return { sorting, toggleSorting, sortedContacts };
};
