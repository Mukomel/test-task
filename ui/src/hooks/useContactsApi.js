import { useState } from 'react';
import axios from 'axios';

const API_URL = process.env.REACT_APP_REST_API;

export default () => {
    const [contacts, setContacts] = useState([]);

    const fetchContacts = async () => {
        const response = await axios.get(`${API_URL}/contacts`);
        setContacts(response.data);
    };

    const createContact = async (name, phone) => {
        try {
            const response = await axios.post(`${API_URL}/contacts`, {
                name,
                phone,
            });
            setContacts(response.data);
        } catch (e) {
            if (e.response.status === 422) {
                throw e.response.data.errorMessage;
            } else {
                throw e;
            }
        }
    };

    const updateContact = async (id, name, phone) => {
        try {
            const response = await axios.put(`${API_URL}/contacts/${id}`, {
                name,
                phone,
            });
            setContacts(response.data);
        } catch (e) {
            if (e.response.status === 422) {
                throw e.response.data.errorMessage;
            } else {
                throw e;
            }
        }
    };

    const deleteContact = async id => {
        const response = await axios.delete(`${API_URL}/contacts/${id}`);
        setContacts(response.data);
    };

    return {
        contacts,
        fetchContacts,
        createContact,
        updateContact,
        deleteContact,
    };
};
