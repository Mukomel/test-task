import PropTypes from 'prop-types';

export const contactPropType = PropTypes.exact({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    phone: PropTypes.string,
});

export const contactsPropType = PropTypes.arrayOf(contactPropType);
