import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import AllContactsPage from './components/AllContactsPage';
import CreateContactPage from './components/CreateContactPage';
import UpdateContactPage from './components/UpdateContactPage';
import useContactsApi from './hooks/useContactsApi';
import styles from './App.module.css';

const App = () => {
    const {
        contacts,
        fetchContacts,
        createContact,
        updateContact,
        deleteContact,
    } = useContactsApi();

    useEffect(() => {
        fetchContacts();
    }, []);

    return (
        <div className={styles.wrapper}>
            <Router>
                <Switch>
                    <Route exact path="/">
                        <AllContactsPage
                            contacts={contacts}
                            onDeleteContact={deleteContact}
                        />
                    </Route>
                    <Route exact path="/create">
                        <CreateContactPage onCreateContact={createContact} />
                    </Route>
                    <Route exact path="/:contactId">
                        <UpdateContactPage
                            contacts={contacts}
                            onUpdateContact={updateContact}
                        />
                    </Route>
                </Switch>
            </Router>
        </div>
    );
};

export default App;
