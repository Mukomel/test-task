import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.css';

const TextField = React.forwardRef(({ label, value, id, onChange }, ref) => (
    <div>
        <label className={styles.label} htmlFor={id}>
            {label}
        </label>
        <input
            id={id}
            ref={ref}
            className={styles.input}
            type="text"
            value={value}
            onChange={e => onChange(e.target.value)}
        />
    </div>
));

TextField.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default TextField;
