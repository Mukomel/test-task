import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import plur from 'plur';
import Header from '../Header';
import ContactsList from '../ContactsList';
import { contactsPropType } from '../../propTypes';
import styles from './styles.module.css';

const AllContactsPage = ({ contacts, onDeleteContact }) => {
    const history = useHistory();

    return (
        <div>
            <Helmet>
                <title>Contacts</title>
            </Helmet>
            <Header
                title="Contacts"
                secondaryTitle={`Showing ${contacts.length} ${plur(
                    'contact',
                    contacts.length
                )}`}
                mainButtonLabel="Create"
                mainButtonOnClick={() => history.push('/create')}
            />
            {contacts.length ? (
                <ContactsList
                    contacts={contacts}
                    onContactEditClick={contactId =>
                        history.push(`/${contactId}`)
                    }
                    onContactDeleteClick={contactId =>
                        onDeleteContact(contactId)
                    }
                />
            ) : (
                <div className={styles.noContactsMessage}>
                    Contacts not found
                </div>
            )}
        </div>
    );
};

AllContactsPage.propTypes = {
    contacts: contactsPropType.isRequired,
    onDeleteContact: PropTypes.func.isRequired,
};

export default AllContactsPage;
