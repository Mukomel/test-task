import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { useHistory, useParams } from 'react-router-dom';
import Header from '../Header';
import ContactForm from '../ContactForm';
import { contactsPropType } from '../../propTypes';

const UpdateContactPage = ({ onUpdateContact, contacts }) => {
    const history = useHistory();
    const { contactId } = useParams();
    const form = createRef();

    const contact = contacts.find(contact => contact.id === contactId);

    const handleFormSubmit = async values => {
        try {
            await onUpdateContact(contactId, values.name, values.phone);
            history.push('/');
        } catch (e) {
            throw e;
        }
    };

    return (
        <div>
            <Helmet>
                <title>Edit contact</title>
            </Helmet>
            <Header
                title="Edit contact"
                mainButtonLabel="Save"
                mainButtonOnClick={() => {
                    form.current.submit();
                }}
                secondaryButtonLabel="Cancel"
                secondaryButtonOnClick={() => history.push('/')}
            />
            {contact && (
                <ContactForm
                    ref={form}
                    onSubmit={handleFormSubmit}
                    initialContact={contact}
                />
            )}
        </div>
    );
};

UpdateContactPage.propTypes = {
    contacts: contactsPropType.isRequired,
    onUpdateContact: PropTypes.func.isRequired,
};

export default UpdateContactPage;
