import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.css';
import { contactsPropType } from '../../propTypes';
import ContactItem from './components/ContactItem';
import ListHeader from './components/ListHeader';
import useContactsSorting from '../../hooks/useContactsSorting';

const ContactsList = ({ contacts, onContactEditClick, onContactDeleteClick }) => {
    const { sortedContacts, sorting, toggleSorting } = useContactsSorting(
        contacts
    );

    return (
        <div className={styles.wrapper}>
            <ListHeader nameSorting={sorting} onNameClick={toggleSorting} />
            {sortedContacts.map(contact => (
                <ContactItem
                    key={contact.id}
                    contact={contact}
                    onEditClick={onContactEditClick}
                    onDeleteClick={onContactDeleteClick}
                />
            ))}
        </div>
    );
};

ContactsList.propTypes = {
    contacts: contactsPropType.isRequired,
    onContactEditClick: PropTypes.func.isRequired,
};

export default ContactsList;
