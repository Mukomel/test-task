import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './styles.module.css';

const ListHeader = ({ onNameClick, nameSorting }) => (
    <div className={styles.wrapper}>
        <div className={styles.nameButtonWrapper}>
            <button className={styles.nameButton} onClick={onNameClick}>
                Name{' '}
                <i
                    className={cn(styles.sortingIcon, 'fa', {
                        'fa-caret-up': nameSorting === 'desc',
                        'fa-caret-down': nameSorting === 'asc',
                    })}
                />
            </button>
        </div>
        <div className={styles.phone}>Phone</div>
    </div>
);

ListHeader.propTypes = {
    nameSorting: PropTypes.oneOf(['asc', 'desc']).isRequired,
    onNameClick: PropTypes.func.isRequired,
};

export default ListHeader;
