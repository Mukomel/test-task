import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './styles.module.css';
import { contactPropType } from '../../../../propTypes';

const ContactItem = ({ contact, onEditClick, onDeleteClick }) => (
    <div className={styles.wrapper}>
        <div className={styles.name}>{contact.name}</div>
        <div className={styles.phoneAndButtons}>
            {contact.phone}
            <button
                className={cn(styles.button, styles.deleteButton)}
                onClick={() => onDeleteClick(contact.id)}
            >
                Delete
            </button>
            <button
                className={styles.button}
                onClick={() => onEditClick(contact.id)}
            >
                Edit
            </button>
        </div>
    </div>
);

ContactItem.propTypes = {
    contact: contactPropType.isRequired,
    onEditClick: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired,
};

export default ContactItem;
