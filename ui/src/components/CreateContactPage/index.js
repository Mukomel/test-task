import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import Header from '../Header';
import ContactForm from '../ContactForm';

const CreateContactPage = ({ onCreateContact }) => {
    const history = useHistory();
    const form = createRef();

    const handleFormSubmit = async values => {
        try {
            await onCreateContact(values.name, values.phone);
            history.push('/');
        } catch (e) {
            throw e;
        }
    };

    return (
        <div>
            <Helmet>
                <title>Create contact</title>
            </Helmet>
            <Header
                title="Create contact"
                mainButtonLabel="Save"
                mainButtonOnClick={() => {
                    form.current.submit();
                }}
                secondaryButtonLabel="Cancel"
                secondaryButtonOnClick={() => history.push('/')}
            />
            <ContactForm ref={form} onSubmit={handleFormSubmit} />
        </div>
    );
};

CreateContactPage.propTypes = {
    onCreateContact: PropTypes.func.isRequired,
};

export default CreateContactPage;
