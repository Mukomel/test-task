import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './styles.module.css';

const Header = ({
    title,
    secondaryTitle,
    mainButtonLabel,
    mainButtonOnClick,
    secondaryButtonLabel,
    secondaryButtonOnClick,
}) => (
    <div className={styles.wrapper}>
        <div>
            <div className={styles.mainTitle}>{title}</div>
            {secondaryTitle && (
                <div className={styles.secondaryTitle}>{secondaryTitle}</div>
            )}
        </div>
        <div className={styles.buttonsWrapper}>
            {secondaryButtonLabel && (
                <button
                    className={cn(styles.button, styles.secondaryButton)}
                    onClick={secondaryButtonOnClick}
                >
                    {secondaryButtonLabel}
                </button>
            )}
            <button
                className={cn(styles.button, styles.mainButton)}
                onClick={mainButtonOnClick}
            >
                {mainButtonLabel}
            </button>
        </div>
    </div>
);

Header.propTypes = {
    title: PropTypes.string.isRequired,
    secondaryTitle: PropTypes.string,
    mainButtonLabel: PropTypes.string.isRequired,
    mainButtonOnClick: PropTypes.func.isRequired,
    secondaryButtonLabel: PropTypes.string,
    secondaryButtonOnClick: PropTypes.func,
};

export default Header;
