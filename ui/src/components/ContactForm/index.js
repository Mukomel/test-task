import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import TextField from '../TextField';
import styles from './styles.module.css';
import { contactPropType } from '../../propTypes';

const ENTER_KEY_CODE = 13;

// This component was implemented as a class to achieve form remote submit using ref.
class ContactForm extends React.Component {
    nameInput = createRef();

    constructor(props) {
        super(props);

        const initialContact = props.initialContact;
        this.state = {
            name: initialContact ? initialContact.name : '',
            phone: initialContact ? initialContact.phone : '',
            error: '',
        };
    }

    componentDidMount = () => {
        this.nameInput.current.focus();
    };

    handleNameChange = value => {
        this.setState({ name: value });
    };

    handlePhoneChange = value => {
        this.setState({ phone: value });
    };

    handleKeyDown = e => {
        if (e.keyCode === ENTER_KEY_CODE) {
            this.submit();
        }
    };

    submit = async () => {
        try {
            await this.props.onSubmit({
                name: this.state.name,
                phone: this.state.phone,
            });
        } catch (e) {
            this.setState({ error: e });
        }
    };

    render() {
        const { name, phone, error } = this.state;

        return (
            <form className={styles.wrapper} onKeyDown={this.handleKeyDown}>
                <div className={styles.field}>
                    <TextField
                        label="Full name"
                        id="name"
                        onChange={this.handleNameChange}
                        value={name}
                        ref={this.nameInput}
                    />
                </div>
                <div className={styles.field}>
                    <TextField
                        label="Phone number"
                        id="Phone"
                        onChange={this.handlePhoneChange}
                        value={phone}
                    />
                </div>
                {error && <div className={styles.error}>{error}</div>}
            </form>
        );
    }
}

ContactForm.propTypes = {
    initialContact: contactPropType,
    onSubmit: PropTypes.func.isRequired,
};

export default ContactForm;
