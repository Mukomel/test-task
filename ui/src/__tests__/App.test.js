import React from 'react';
import axiosMock from 'axios';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { render } from '@testing-library/react';
import App from '../App';

jest.mock('axios');

describe('Contacts', () => {
    it('Should display list of contacts', async () => {
        axiosMock.get.mockResolvedValueOnce({
            data: [
                { name: 'John Smith', phone: '111111', id: '1' },
                { name: 'Joe Doe', phone: '222222', id: '2' },
            ],
        });
        const { findByText, getByText } = render(<App />);

        await findByText('John Smith');
        getByText('111111');
        getByText('Joe Doe');
        getByText('222222');
        getByText('Showing 2 contacts');
    });

    it('Should display placeholder if contacts are missing', async () => {
        axiosMock.get.mockResolvedValueOnce({
            data: [],
        });
        const { findByText, getByText } = render(<App />);

        await findByText('Contacts not found');
        getByText('Showing 0 contacts');
    });

    it('Should allow user to add new contact', async () => {
        axiosMock.get.mockResolvedValueOnce({
            data: [],
        });
        const { getByLabelText, getByText, findByText } = render(<App />);
        axiosMock.post.mockResolvedValueOnce({
            data: [{ name: 'Jaden Kim', phone: '777777', id: '1' }],
        });

        userEvent.click(getByText('Create'));

        await findByText('Create contact');
        await userEvent.type(getByLabelText('Full name'), 'Jaden Kim');
        await userEvent.type(getByLabelText('Phone number'), '777777');
        userEvent.click(getByText('Save'));

        expect(axiosMock.post).toBeCalledWith(expect.any(String), {
            name: 'Jaden Kim',
            phone: '777777',
        });

        await findByText('Showing 1 contact');
        getByText('Jaden Kim');
        getByText('777777');
    });
});
