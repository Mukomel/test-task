const getAllContacts = async (req, res) => {
    const allContacts = await req.db.findContacts();
    res.send(allContacts);
};

const createContact = async (req, res) => {
    try {
        const allContacts = await req.db.createContact({
            name: req.body.name,
            phone: req.body.phone,
        });
        res.send(allContacts);
    } catch (e) {
        res.status(422).send({ errorMessage: e.message });
    }
};

const updateContact = async (req, res) => {
    try {
        const allContacts = await req.db.editContact(req.params.contactId, {
            name: req.body.name,
            phone: req.body.phone,
        });
        res.send(allContacts);
    } catch (e) {
        res.status(422).send({ errorMessage: e.message });
    }
};

const deleteContact = async (req, res) => {
    const allContacts = await req.db.deleteContact(req.params.contactId);
    res.send(allContacts);
};

module.exports = {
    getAllContacts,
    createContact,
    updateContact,
    deleteContact,
};
