const request = require('supertest');
const createServer = require('../createServer');

const contacts = [
    { id: '1', name: 'John Smith', phone: '111111' },
    { id: '2', name: 'Joe Doe', phone: '222222' },
];

const db = {
    findContacts: async () => contacts,
    createContact: async contact => [
        ...contacts,
        { ...contact, id: 'new-contact=id' },
    ],
};
const app = createServer(db);

describe('Contacts api', () => {
    it('Should return list of all contacts', async () => {
        const res = await request(app).get('/api/contacts');

        expect(res.body).toEqual(contacts);
    });

    it('Should create new contact and return list of all contacts', async () => {
        const contact = { name: 'Jaden Kim', phone: '333333' };
        const res = await request(app)
            .post('/api/contacts')
            .send(contact);

        expect(res.body).toEqual([
            ...contacts,
            { name: 'Jaden Kim', phone: '333333', id: 'new-contact=id' },
        ]);
    });
});
