const express = require('express');
const contactsController = require('../controllers/contacts');

const router = express.Router();

router
    .route('/')
    .get(contactsController.getAllContacts)
    .post(contactsController.createContact);

router
    .route('/:contactId')
    .put(contactsController.updateContact)
    .delete(contactsController.deleteContact);

module.exports = router;
