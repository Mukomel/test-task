const createServer = require('./createServer');
const database = require('./lib/database');
const port = 9898;

const server = createServer(database);

server.listen(port, () =>
    console.log(`Example app listening on port ${port}!`)
);
