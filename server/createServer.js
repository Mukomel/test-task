const express = require('express');
const bodyParser = require('body-parser');
const cors = require('./lib/cors');
const apiRouter = require('./routes');

const createServer = (db) => {
    const app = express();

    app.use(bodyParser.json());
    app.use(cors);
    app.use((req, res, next) => {
        req.db = db;
        next();
    });
    app.use('/api', apiRouter);

    return app;
};

module.exports = createServer;
