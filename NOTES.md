# Notes
* Since design was provided in form of a mockup, I was not trying to implement pixel-perfect solution. Hence, colors/margins/paddings was selected approximately to look alike the mockup.
* I have slightly updated `database.js` file because some of it's methods were not returning Promises.

# Next steps
There are a list of some improvements I would implement to polish current implementation: 

* Add more tests
* Add client side validation and formatting to phone number input and display
* Add loading states to client side (show spinner while data is fetching, disable "Save" button while form is submitting)
* Performance improvements (check for unnecessary rerenders, use useMemo/useCallback hooks)
* Consider usage of more advanced approach to data fetching and storage (redux, apollo-react) in case if of application scale. 
* Typescript usage